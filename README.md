# uniparc-domain

[![docs](https://img.shields.io/badge/docs-v0.2-blue.svg)](https://datapkg.gitlab.io/uniparc-domain/)
[![build status](https://gitlab.com /datapkg/uniparc-domain/badges/master/build.svg)](https://gitlab.com /datapkg/uniparc-domain/commits/master/)

Add domain sequences to `uniparc_domain.parquet`.

## Notebooks
