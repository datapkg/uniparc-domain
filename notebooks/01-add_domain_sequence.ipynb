{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Summary\n",
    "\n",
    "In this notebook we add domain length and domain sequence columns to the `uniparc_domain` table that we obtain in the `uniparc` datapkg."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Imports"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python Packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spark Server"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "            <div>\n",
       "                <p><b>SparkSession - in-memory</b></p>\n",
       "                \n",
       "        <div>\n",
       "            <p><b>SparkContext</b></p>\n",
       "\n",
       "            <p><a href=\"http://beagle10.kimlab.med.utoronto.ca:4040\">Spark UI</a></p>\n",
       "\n",
       "            <dl>\n",
       "              <dt>Version</dt>\n",
       "                <dd><code>v2.3.1</code></dd>\n",
       "              <dt>Master</dt>\n",
       "                <dd><code>local[64]</code></dd>\n",
       "              <dt>AppName</dt>\n",
       "                <dd><code>uniparc-domain</code></dd>\n",
       "            </dl>\n",
       "        </div>\n",
       "        \n",
       "            </div>\n",
       "        "
      ],
      "text/plain": [
       "<pyspark.sql.session.SparkSession at 0x7ff2f214fc50>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%run spark.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "NOTEBOOK_NAME = 'add_domain_sequence'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PosixPath('/home/kimlab2/database_data/databin/uniparc/v0.1')"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "UNIPARC_PATH = Path(os.environ['DATAPKG_OUTPUT_DIR']).joinpath('uniparc').joinpath('v0.1').resolve()\n",
    "UNIPARC_PATH"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "PosixPath('/home/kimlab1/database_data/datapkg_output_dir/uniparc-domain/v0.2')"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "OUTPUT_PATH = (\n",
    "    Path(os.environ['DATAPKG_OUTPUT_DIR'])\n",
    "    .joinpath(os.environ['PROJECT_NAME'])\n",
    "    .joinpath(f\"v{os.environ['PROJECT_VERSION']}\")\n",
    ")\n",
    "os.makedirs(OUTPUT_PATH, exist_ok=True)\n",
    "OUTPUT_PATH"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Explore"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = spark.sql(f\"\"\"\\\n",
    "SELECT *\n",
    "FROM parquet.`{UNIPARC_PATH}/uniparc_domain.parquet`\n",
    "LIMIT 10\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-------------+--------+-----------------+--------------------+-----------+------------+----------+-----------------+\n",
      "|   uniparc_id|database|      database_id|       interpro_name|interpro_id|domain_start|domain_end|__index_level_0__|\n",
      "+-------------+--------+-----------------+--------------------+-----------+------------+----------+-----------------+\n",
      "|UPI0000000011|  ProDom|         PD012198|         Poxvirus I5|  IPR006803|          11|        75|                0|\n",
      "|UPI0000000011|    Pfam|          PF04713|         Poxvirus I5|  IPR006803|           3|        75|                1|\n",
      "|UPI0000000011|   PIRSF|      PIRSF003768|         Poxvirus I5|  IPR006803|           1|        79|                2|\n",
      "|UPI0000000066|  Gene3D|G3DSA:3.10.290.10|RNA-binding S4 do...|  IPR036986|          35|       120|                3|\n",
      "|UPI0000000066|  Gene3D|G3DSA:3.10.290.10|RNA-binding S4 do...|  IPR036986|         137|       185|                4|\n",
      "|UPI0000000066|   HAMAP|         MF_00485|Ribosomal protein...|  IPR000876|           7|       238|                5|\n",
      "|UPI0000000066|  ProDom|         PD002667|Ribosomal protein...|  IPR013845|          87|       181|                6|\n",
      "|UPI0000000066|    Pfam|          PF00467|                 KOW|  IPR005824|         178|       211|                7|\n",
      "|UPI0000000066|    Pfam|          PF00900|Ribosomal protein...|  IPR013845|          95|       169|                8|\n",
      "|UPI0000000066|    Pfam|          PF01479|RNA-binding S4 do...|  IPR002942|          44|        90|                9|\n",
      "+-------------+--------+-----------------+--------------------+-----------+------------+----------+-----------------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "ds.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_ds = spark.sql(f\"\"\"\\\n",
    "SELECT *\n",
    "FROM parquet.`{OUTPUT_PATH}/{NOTEBOOK_NAME}/uniparc_domain.parquet`\n",
    "LIMIT 10\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-------------+--------------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+-------------+\n",
      "|   uniparc_id|            sequence|database|       database_id|       interpro_name|interpro_id|domain_start|domain_end|__index_level_0__|domain_length|\n",
      "+-------------+--------------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+-------------+\n",
      "|UPI0005007D0B|AAAAAAAAAQQPQTPAP...|  Gene3D| G3DSA:2.130.10.10|WD40/YVTN repeat-...|  IPR015943|         285|       720|         47843740|          436|\n",
      "|UPI0007E73D35|AAAAAAAAAQQQHAQQQ...|  Gene3D| G3DSA:2.130.10.10|WD40/YVTN repeat-...|  IPR015943|          30|       373|        528721673|          344|\n",
      "|UPI0006D326CC|AAAAAAAAGQQQQHQLQ...|  Gene3D|  G3DSA:1.25.10.10|Armadillo-like he...|  IPR011989|         456|      1112|         22150375|          657|\n",
      "|UPI000628E541|AAAAAAADISSKDFRLG...|  Gene3D| G3DSA:3.90.550.10|Nucleotide-diphos...|  IPR029044|          42|       386|        105351091|          345|\n",
      "|UPI000007C110|AAAAAAHQTTHLHHHHH...|  Gene3D| G3DSA:1.10.565.10|Nuclear hormone r...|  IPR035500|          11|       365|        222874656|          355|\n",
      "|UPI0009011670|AAAAAAQRKSSSSRRNA...|  Gene3D|  G3DSA:1.10.10.10|Winged helix-like...|  IPR036388|         157|       262|        810957168|          106|\n",
      "|UPI00043E9006|AAAAAASDETLAAIFAQ...|  Gene3D|  G3DSA:1.25.10.10|Armadillo-like he...|  IPR011989|           8|       153|        650222863|          146|\n",
      "|UPI000296AAB4|AAAAAAVDAATRYHNRT...|  Gene3D| G3DSA:3.40.109.10| Nitroreductase-like|  IPR000415|          31|       274|        409402566|          244|\n",
      "|UPI00081AE6C9|AAAAAEEAVAAAATSGK...|  Gene3D| G3DSA:3.40.640.10|Pyridoxal phospha...|  IPR015421|          92|       327|        318478501|          236|\n",
      "|UPI0003F43178|AAAAAFRDGEDDAAQMR...|  Gene3D|G3DSA:3.90.1150.10|Pyridoxal phospha...|  IPR015422|         277|       387|        340976315|          111|\n",
      "+-------------+--------------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+-------------+\n",
      "\n"
     ]
    }
   ],
   "source": [
    "output_ds.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'/home/kimlab2/database_data/databin/uniparc/uniparc.parquet'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f\"{DATABIN}/uniparc/uniparc.parquet\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "uniparc = spark.read.parquet(f\"{DATABIN}/uniparc/uniparc.parquet\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "uniparc_domain = spark.read.parquet(f\"{DATABIN}/uniparc/uniparc_domain.parquet\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['id', 'sequence', 'sequence_length', 'sequence_checksum', '__index_level_0__']"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "uniparc.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['uniparc_id',\n",
       " 'database',\n",
       " 'database_id',\n",
       " 'interpro_name',\n",
       " 'interpro_id',\n",
       " 'domain_start',\n",
       " 'domain_end',\n",
       " '__index_level_0__']"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "uniparc_domain.columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pyspark.sql.functions as F\n",
    "from pyspark.sql.functions import col"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "+-------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+\n",
      "|   uniparc_id|database|       database_id|       interpro_name|interpro_id|domain_start|domain_end|__index_level_0__|\n",
      "+-------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+\n",
      "|UPI0000000011|  ProDom|          PD012198|         Poxvirus I5|  IPR006803|          11|        75|                0|\n",
      "|UPI0000000011|    Pfam|           PF04713|         Poxvirus I5|  IPR006803|           3|        75|                1|\n",
      "|UPI0000000011|   PIRSF|       PIRSF003768|         Poxvirus I5|  IPR006803|           1|        79|                2|\n",
      "|UPI0000000066|  Gene3D| G3DSA:3.10.290.10|RNA-binding S4 do...|  IPR036986|          35|       120|                3|\n",
      "|UPI0000000066|  Gene3D| G3DSA:3.10.290.10|RNA-binding S4 do...|  IPR036986|         137|       185|                4|\n",
      "|UPI0000000066|   HAMAP|          MF_00485|Ribosomal protein...|  IPR000876|           7|       238|                5|\n",
      "|UPI0000000066|  ProDom|          PD002667|Ribosomal protein...|  IPR013845|          87|       181|                6|\n",
      "|UPI0000000066|    Pfam|           PF00467|                 KOW|  IPR005824|         178|       211|                7|\n",
      "|UPI0000000066|    Pfam|           PF00900|Ribosomal protein...|  IPR013845|          95|       169|                8|\n",
      "|UPI0000000066|    Pfam|           PF01479|RNA-binding S4 do...|  IPR002942|          44|        90|                9|\n",
      "|UPI0000000066|    Pfam|           PF08071|Ribosomal protein...|  IPR013843|           3|        39|               10|\n",
      "|UPI0000000066|    Pfam|           PF16121|40S ribosomal pro...|  IPR032277|         212|       259|               11|\n",
      "|UPI0000000066|   PIRSF|       PIRSF002116|Ribosomal protein...|  IPR000876|           1|       241|               12|\n",
      "|UPI0000000066| PROSITE|           PS00528|Ribosomal protein...|  IPR018199|           8|        22|               13|\n",
      "|UPI0000000066| PROSITE|           PS50889|RNA-binding S4 do...|  IPR002942|          42|       104|               14|\n",
      "|UPI0000000066| PANTHER|         PTHR11581|Ribosomal protein...|  IPR000876|           1|       263|               15|\n",
      "|UPI0000000066|   SMART|           SM00363|RNA-binding S4 do...|  IPR002942|          42|       106|               16|\n",
      "|UPI0000000066|     CDD|           cd00165|RNA-binding S4 do...|  IPR002942|          51|       112|               17|\n",
      "|UPI00000000CD|    Pfam|           PF03221|HTH CenpB-type DN...|  IPR006600|          74|       125|               18|\n",
      "|UPI00000000CD|    Pfam|           PF04218|DNA binding HTH d...|  IPR007889|           3|        55|               19|\n",
      "|UPI00000000CD| PROSITE|           PS50960|DNA binding HTH d...|  IPR007889|           1|        52|               20|\n",
      "|UPI00000000CD| PROSITE|           PS51253|HTH CenpB-type DN...|  IPR006600|          65|       125|               21|\n",
      "|UPI00000000CD| PANTHER|         PTHR45553|Major centromere ...|  IPR033062|           1|       125|               22|\n",
      "|UPI00000000CD|   SMART|           SM00674|HTH CenpB-type DN...|  IPR006600|          71|       125|               23|\n",
      "|UPI00000000CD|  SUPFAM|          SSF46689|Homeobox domain-like|  IPR009057|           1|        65|               24|\n",
      "|UPI00000000CD|  SUPFAM|          SSF46689|Homeobox domain-like|  IPR009057|          72|       125|               25|\n",
      "|UPI000000014B|    Pfam|           PF00001|G protein-coupled...|  IPR000276|          52|       303|               26|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|          36|        60|               27|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|          69|        90|               28|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|         114|       136|               29|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|         149|       170|               30|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|         201|       224|               31|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|         247|       271|               32|\n",
      "|UPI000000014B|  PRINTS|           PR00237|G protein-coupled...|  IPR000276|         285|       311|               33|\n",
      "|UPI000000014B|  PRINTS|           PR00238|               Opsin|  IPR001760|          57|        69|               34|\n",
      "|UPI000000014B|  PRINTS|           PR00238|               Opsin|  IPR001760|         171|       183|               35|\n",
      "|UPI000000014B|  PRINTS|           PR00238|               Opsin|  IPR001760|         282|       294|               36|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|           4|        12|               37|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|          13|        25|               38|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|         190|       200|               39|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|         200|       211|               40|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|         267|       286|               41|\n",
      "|UPI000000014B|  PRINTS|           PR00574|Opsin, blue sensi...|  IPR001521|         314|       328|               42|\n",
      "|UPI000000014B| PROSITE|           PS00237|G protein-coupled...|  IPR000276|         120|       136|               43|\n",
      "|UPI000000014B| PROSITE|           PS00238|Visual pigments (...|  IPR027430|         287|       303|               44|\n",
      "|UPI000000014B| PROSITE|           PS50262|GPCR, rhodopsin-l...|  IPR017452|          51|       303|               45|\n",
      "|UPI0000000164|  Gene3D|G3DSA:1.10.3430.10|Ammonium/urea tra...|  IPR029020|           1|        54|               46|\n",
      "|UPI0000000164|    Pfam|           PF03253|    Urea transporter|  IPR004937|           1|        54|               47|\n",
      "|UPI0000000164| PANTHER|         PTHR10464|    Urea transporter|  IPR004937|           1|        54|               48|\n",
      "|UPI0000000230|    Pfam|           PF17057|Orthopoxvirus B3 ...|  IPR031450|           1|       123|               49|\n",
      "|UPI0000000230| PANTHER|         PTHR12155|     Schlafen family|  IPR029684|           8|       121|               50|\n",
      "|UPI000000023A|  Gene3D|  G3DSA:4.10.10.10|Metallothionein d...|  IPR023587|           1|        60|               51|\n",
      "|UPI000000023A|    Pfam|           PF00131|     Metallothionein|  IPR003019|           1|        61|               52|\n",
      "|UPI000000023A|  PRINTS|           PR00860|Metallothionein, ...|  IPR000006|           4|        16|               53|\n",
      "|UPI000000023A|  PRINTS|           PR00860|Metallothionein, ...|  IPR000006|          27|        40|               54|\n",
      "|UPI000000023A|  PRINTS|           PR00860|Metallothionein, ...|  IPR000006|          41|        50|               55|\n",
      "|UPI000000023A| PROSITE|           PS00203|Metallothionein, ...|  IPR018064|          12|        30|               56|\n",
      "|UPI000000023A| PANTHER|         PTHR23299|Metallothionein, ...|  IPR000006|           1|        61|               57|\n",
      "|UPI000000023A|  SUPFAM|          SSF57868|Metallothionein d...|  IPR017854|           1|        61|               58|\n",
      "|UPI00000002BA|  ProDom|          PD002978|Geminivirus V2 pr...|  IPR002511|           1|        60|               59|\n",
      "|UPI00000002BA|    Pfam|           PF01524|Geminivirus V2 pr...|  IPR002511|           1|        78|               60|\n",
      "|UPI00000002BA|    Pfam|           PF03716|          WCCH motif|  IPR005159|          79|       103|               61|\n",
      "|UPI00000002C2|    Pfam|           PF00008|     EGF-like domain|  IPR000742|          39|        71|               62|\n",
      "|UPI00000002C2| PROSITE|           PS00022|EGF-like, conserv...|  IPR013032|          61|        72|               63|\n",
      "|UPI00000002C2| PROSITE|           PS50026|     EGF-like domain|  IPR000742|          29|        73|               64|\n",
      "|UPI00000002CE|  Gene3D|G3DSA:3.40.50.1820|Alpha/Beta hydrol...|  IPR029058|         131|       408|               65|\n",
      "|UPI00000002CE|    Pfam|           PF04775|Acyl-CoA thioeste...|  IPR006862|          16|       141|               66|\n",
      "|UPI00000002CE|    Pfam|           PF08840|BAAT/Acyl-CoA thi...|  IPR014940|         203|       409|               67|\n",
      "|UPI00000002CE|   PIRSF|       PIRSF016521|Acyl-CoA thioeste...|  IPR016662|           1|       414|               68|\n",
      "|UPI00000002CE|  SUPFAM|          SSF53474|Alpha/Beta hydrol...|  IPR029058|          66|       363|               69|\n",
      "|UPI00000002CE|  SUPFAM|          SSF53474|Alpha/Beta hydrol...|  IPR029058|         394|       409|               70|\n",
      "|UPI000000038C|    Pfam|           PF16276|Nucleophosmin, C-...|  IPR032569|           1|        33|               71|\n",
      "|UPI000000041A|    Pfam|           PF01555|DNA methylase N-4...|  IPR002941|          43|       258|               72|\n",
      "|UPI000000041A|  PRINTS|           PR00508|Restriction/modif...|  IPR001091|          41|        55|               73|\n",
      "|UPI000000041A|  PRINTS|           PR00508|Restriction/modif...|  IPR001091|          89|       109|               74|\n",
      "|UPI000000041A|  PRINTS|           PR00508|Restriction/modif...|  IPR001091|         200|       217|               75|\n",
      "|UPI000000041A|  PRINTS|           PR00508|Restriction/modif...|  IPR001091|         219|       237|               76|\n",
      "|UPI000000041A|  PRINTS|           PR00508|Restriction/modif...|  IPR001091|         242|       262|               77|\n",
      "|UPI000000041A| PROSITE|           PS00092|DNA methylase, N-...|  IPR002052|          45|        51|               78|\n",
      "|UPI000000041A|  SUPFAM|          SSF53335|S-adenosyl-L-meth...|  IPR029063|          17|       263|               79|\n",
      "|UPI0000000464|    Pfam|           PF10723|Replication regul...|  IPR019661|           1|        82|               80|\n",
      "|UPI000000049B|  Gene3D|  G3DSA:3.60.21.10|Metallo-dependent...|  IPR029052|           1|        57|               81|\n",
      "|UPI000000049B|  PRINTS|           PR00114|Serine/threonine-...|  IPR006186|          30|        50|               82|\n",
      "|UPI000000049B|  PRINTS|           PR00114|Serine/threonine-...|  IPR006186|          52|        57|               83|\n",
      "|UPI000000049B|  SUPFAM|          SSF56300|Metallo-dependent...|  IPR029052|           2|        57|               84|\n",
      "|UPI0000000536|    Pfam|           PF00046|     Homeobox domain|  IPR001356|         123|       167|               85|\n",
      "|UPI0000000536|    Pfam|           PF00412|Zinc finger, LIM-...|  IPR001781|          19|        73|               86|\n",
      "|UPI0000000536| PROSITE|           PS00478|Zinc finger, LIM-...|  IPR001781|          19|        54|               87|\n",
      "|UPI0000000536| PROSITE|           PS50023|Zinc finger, LIM-...|  IPR001781|          17|        79|               88|\n",
      "|UPI0000000536| PROSITE|           PS50071|     Homeobox domain|  IPR001356|         119|       167|               89|\n",
      "|UPI0000000536|   SMART|           SM00132|Zinc finger, LIM-...|  IPR001781|          18|        72|               90|\n",
      "|UPI0000000536|  SUPFAM|          SSF46689|Homeobox domain-like|  IPR009057|         108|       167|               91|\n",
      "|UPI0000000536|     CDD|           cd00086|     Homeobox domain|  IPR001356|         123|       167|               92|\n",
      "|UPI0000000561|  Gene3D|  G3DSA:2.30.30.30|Ribosomal protein...|  IPR014722|           1|        33|               93|\n",
      "|UPI0000000561|  ProDom|          PD009396|Ribosomal protein...|  IPR001141|           1|        40|               94|\n",
      "|UPI0000000561|    Pfam|           PF01777|Ribosomal protein...|  IPR001141|           1|        40|               95|\n",
      "|UPI0000000561| PROSITE|           PS01107|Ribosomal protein...|  IPR018262|          29|        40|               96|\n",
      "|UPI000000059A|  Gene3D|  G3DSA:2.60.40.10|Immunoglobulin-li...|  IPR013783|          29|        97|               97|\n",
      "|UPI000000059A|  Gene3D|  G3DSA:2.60.40.10|Immunoglobulin-li...|  IPR013783|         113|       180|               98|\n",
      "|UPI000000059A|  Gene3D|  G3DSA:2.60.40.10|Immunoglobulin-li...|  IPR013783|         206|       286|               99|\n",
      "+-------------+--------+------------------+--------------------+-----------+------------+----------+-----------------+\n",
      "only showing top 100 rows\n",
      "\n"
     ]
    }
   ],
   "source": [
    "uniparc_domain.show(100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "_uniparc_domain_columns = [\n",
    "    'uniparc_id', 'database', 'database_id', 'interpro_name', 'interpro_id', \n",
    "    'domain_start', 'domain_end', '__index_level_0__'\n",
    "]\n",
    "\n",
    "df = (\n",
    "    uniparc\n",
    "    .select('id', 'sequence')\n",
    "    .withColumnRenamed('id', 'uniparc_id')\n",
    "    .join(\n",
    "        uniparc_domain\n",
    "        .filter(col('database') == 'Gene3D')\n",
    "        .select(*_uniparc_domain_columns),\n",
    "        on='uniparc_id', how='inner') \n",
    "    .withColumn('domain_length', col('domain_end') - col('domain_start') + 1) \n",
    "    .withColumn('sequence', col('sequence').substr(col('domain_start'), col('domain_length'))\n",
    "    .dropDuplicates(subset=['sequence'])\n",
    ")\n",
    "# df = df.withColumn('domain_sequence', substring(df['sequence'], df['domain_start'], df['domain_length']))\n",
    "# df = df.withColumn('domain_sequence', substring(col('sequence'), col('domain_start'), col('domain_length')))\n",
    "# df = df.dropDuplicates(subset=['sequence'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "result = (\n",
    "    df.write\n",
    "    .format(\"parquet\")\n",
    "    .save(f\"{NOTEBOOK_NAME}/uniparc_domain_wsequence.parquet\")\n",
    ")\n",
    "result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#     .partitionBy(\"database_id\")\n",
    "#     .mode(\"overwrite\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#                 F.expr(\"SUBSTRING(sequence, domain_start, domain_length)\"))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "result = spark.sql(f\"\"\"\\\n",
    "select *\n",
    "from parquet.`{NOTEBOOK_NAME}/uniparc_domain.parquet`\n",
    "limit 10\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[Row(uniparc_id='UPI0005CA489C', sequence='AAAAATAAAAVVTSCATAAQPGLHPGQLLTARPLTTTAALPSGETRLITYVSDDAHGQPIVVSGTVAVPKSPPPEDGWPVISWAHGTTGYADTCAPSVDTPDGLAHDYLGPVTTMLDTWVARGYAVVQTDYQGLGTPGGHPYVDGTSEANTVTDIVRAARELNSGIGTDWVAIGHSQGGQAALFTAQDGGQRAPELDLKGAAALAPGGVGMSQAVDFVRSGSPDAVGTQAFLPLLVLGAAVVDPGIEPDEIFTPQAQPLLTAARTACIGQIRAVAPVPSAQVFAADANVTPLQDYLGRQDPTRLSPGVPVMIAQGTADTAVTQPGVDALAKAMCDKDVAVDYRVYAGQDHRGVIGASLPDVQDFVNTVMDGKTVNT', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=8, domain_end=383, __index_level_0__=848707595, domain_length=376, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI0001ECBB9F', sequence='AAAAGFKTKASITNFGGQLLKLSHKSSTTGTDMAVNLYLPPQCTTRAVPVLFYLSGLTCTPDNCTEKGFFQAQASVRGVAIVYPDTSPRGLDLLGERDSWDFGEAASFYVDATREPFRQHYRMETYITKELPELLFREFAGKLDRSRVSITGHSMGGHGALSLYLRHPGMYRSVSAFAPIANPSQCPWGEKAFTGYLGDDRAEWAKHDSTELVKNWSYGPLNALIDVGLGDNFYIQKQLLPENFEKAVKEKNLEGLNLRYHYGYDHSYFFMASFSKDHVDHAAKHLG', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=2, domain_end=288, __index_level_0__=633257234, domain_length=287, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI000996F4A9', sequence='AAAALVTLAGAGTWNALASEGQPPVHRADRVLDMPGAQIDTSYFTSGSRSEKRPAVLLGHGFGGSKDGVRAQAQKLAREGYAVLTWSARGFGASTGQIGLNDPKHEVADVSRLIDWLAERPEVRLDETGDPRVGVAGASYGGAIALLAAGHDERVDALAPRMTYWNLADALFPDGVFKKLWAGVFFGSGSPEGGCGRFEAELCEMYERVAVSGKPDAAARELLAARSPSAVGDRIKVPTLIAQGQSDSLFPLGQADAMAKAIRANGAPVSVDWFAGGHDGGDLETARLDTRTTAWF', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=32, domain_end=327, __index_level_0__=883056292, domain_length=296, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI00045B3BF4', sequence='AAAANSYADAEAAIASTRQNQLAVPAAAPTPAAAAMIPPFPANLTTLFFGPTGIPLPPPSMLTPPIRCRSVRRALQAVFTPEELYPLTGVRSLVLNTSVEEGLTILHDAIMVELATTGNAVNVFGWSQSAIIASLEMQRFTAMGGAAPSASDLNFVLVGNEMNPNGGMLARFPDLTLPTLDLTFYGATPSDTIYPTAIYTLEYDGFADFSRYPLNFISDLNAVAGITFVHTKYLDLTPAQVEGATKL', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=37, domain_end=283, __index_level_0__=692447891, domain_length=247, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI0006DC1BC1', sequence='AAAANSYADAEAAIASTRQNQLAVPAAAPTPAAAAMIPPFPANLTTLFFGPTGIPLPPPSMLTPPIRCRSVRRALQAVFTPEELYPLTGVRSLVLNTSVEEGLTILHDAIMVELATTGNAVTVFGWSESAIIASLEMQRFTAMGGAAPSASDLNFVLVGNEMNPNGGMLARFPDLTLPTLDLTFYGATPSDTIYPTAIYTLEYDGFADFSRYPLNFISDLNAVAGITFVHTKYLDLTPAQVEGATKL', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=37, domain_end=283, __index_level_0__=639568864, domain_length=247, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI0009BE8366', sequence='AAAAPAAGSAPAASSKAAVPKGLESFYGQKVAWYDCAATGGMEKSAEKTGFQCAKVKVPLDYSKPDGQTIEIAMKKHQATGSVRQGTLFVNPGGPGYSGVEMVENNEIQFSPALNQAYDIIGFDPRGVGSSTPITCDDGAGQQPAKAAQGGMGVNDPQPGSLVADVAGGDDPTPFRDAENPAADGGAEANVGFPTLIDEITKDFKQEEANCAAHTKPAGLLDHVDTVSVARDLDVLRALSGDQKLNYLGFSYGTYLGAHYAEHFPANTGRMVLDGALDPSLSLGDRAAGQAQGFEAALRTYVQQCQSGQAVQEGQSCPLTGDADAGVQQVRALIASADQTPLKTADPNVTVDGSTIRSAIRRLLYSSEYWPLLTYALDQAITQKDGSYIQVLYGKVTAGGSAPTFYAVNCQDIPVQGDMTSWEKEYQQILQSSPTFGASLGNQDARCQAWGHNGTRKPAPIHAKGAAPILVVGTTGDPATPYAWSQAMADQLESGRLLTWEGNGHTAYGRAGACVQKAVDTYLTSG', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=19, domain_end=544, __index_level_0__=755779261, domain_length=526, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI0005BEA6F9', sequence='AAAAPSYGPELEGFDYPYPVKRHAVASQGQDLSMAYMDVVPERPNGGTIVLLHGKNFCAATWGDTAGVLARAGYRVLALDQIGFCKSSKPAGYQFSFQQLATNTHGLLASLGIAKATIVGHSMGGMLAARYALMFPEAVDQLVMVDPLGLEDWQAKGVPYQTIDAAYAAERRTSAASIKAYQLKFYYDGQWKPAYDRWVDMQAGLSAGPGAERVALNQAQTSDMVFTQPVVHELDRLRVPTVLMAGTQDRTAPGANRATPEVAATLGHYDRLAPEVAGRIKGARLVMFEGLGHAPQVEAPDRFHEALLRELAAR', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=16, domain_end=329, __index_level_0__=497677163, domain_length=314, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI0003B5C5D8', sequence='AAAASPVTREGKPVSLVPTDQQTKKPSSATIPLWPGPAPGSPPPPELPSPKLKQTPVTGGKEYRLKGIAAPVLFAYRPAQPNGAAIIVMPGGGFTFLSIENEGSVPATFFTDFGYTVFVLSYRLPGEGWTNRADAPLQDAIRAIRLVRDQAKAYAIDPDRVAVLGFSAGGHVAASLATDYGTPLYPPIDAADKQSAKPALVGLMYAVTTMKKFETHSVSRANLLGPDPAPDLVQRRSPLAHIGAGTPPCFVGCALDDPVVPPFCSIDWLAACRTAKVPVEGHFFEKGGHGFGLRLSTDNPGALWPELFRLWM', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=13, domain_end=324, __index_level_0__=452832391, domain_length=312, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI000579C517', sequence='AAAATATAGHFGITSRPEESMAPANPSFGPDGVATKDIHIDPLTSLSLRIFLPDSSLPRPKPHPAVRSLDPPRRNSCGGNAAPANDRAARRSSFDAPRSAAENGVGGGSYRGYLPSVAGDGRHRSGGSRKLPVILQFHGGGFIAGSNTCAANDVFCRRIAKLCDVIVIAVGYRLAPENRYPAAFEDGLEVLNWLGKQANLAECSKSLGDVKGGGDGDSKRADIVDTFGASTVEPWLAAHADPSRCVLLGVSCGANIADFVARKAVEAGKLIEPVKVVAQVLMYPFFIGSVPTHSEIKLANSYFYDKSMCVLAWRLFLPEAEYSLDHPAANPLVPGRGPPLKCMPPTLTVVAEHDWMRDRAIAYSEELRKVNVDAPVLEYKDAVHEFATLDMLLKTPQAQACAEDIAIWVKKYISIRGH', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=29, domain_end=446, __index_level_0__=525441804, domain_length=418, database_id='G3DSA:3.40.50.1820'),\n",
       " Row(uniparc_id='UPI000AD86273', sequence='AAAAVLMGPTSVAFAQPEAPGAVSSVDHIEKINDRRWDVHVYSASMDRVIPLQVLRPADTSAPRPTLYMLNGAGGGEDGANWLKQTDMPEFFADKNVNVVIPVGGYLSYYTDWEHDDPGLGRNKWQTFLTEELPPVLDDALGANGVNAIGGLSMSAGSVLDLAIQAPGLYRGVASYSGCAQTSDPVARNFIRTLIAVRGKGDTDNMWGPDGDPRWVEHDPYVNAEGLRGLELFVSNATGLPGPHEMPNAVRPVGSPPLQEQILLGGVIEAVANGCAERLQQRLGELGIPAQFDLGHPGTHSWLYWQDALRASWPTLERALAG', database='Gene3D', interpro_name='Alpha/Beta hydrolase fold', interpro_id='IPR029058', domain_start=12, domain_end=333, __index_level_0__=841287189, domain_length=322, database_id='G3DSA:3.40.50.1820')]"
      ]
     },
     "execution_count": 85,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "result.take(10)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": false,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "304px",
    "left": "1652.57px",
    "right": "20px",
    "top": "110.33px",
    "width": "254px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
