#!/usr/bin/env python3
import os
import sys
from pathlib import Path

import yaml

PROJECT_ROOT = Path(__file__).absolute().parent.parent

if __name__ == "__main__":
    stage = sys.argv[-1] if len(sys.argv) > 1 else None

    with PROJECT_ROOT.joinpath(".gitlab-ci.yml").open("rt") as fin:
        ci_data = yaml.load(fin)

    if stage is None:
        variables = {**ci_data.get("variables", {})}
    else:
        if stage not in ci_data:
            raise Exception(f"No stage with name '{stage}'!")
        variables = {
            **ci_data.get("variables", {}),
            **ci_data[sys.argv[1]].get("variables", {}),
        }

    if os.getenv('CI'):
        variables['CONDA_ENV_NAME'] = variables['PROJECT_NAME'] + '-ci'
    else:
        variables['CONDA_ENV_NAME'] = variables['PROJECT_NAME']

    for key, value in variables.items():
        print(f"export {key}='{value}'")

    print(f"source activate {variables['CONDA_ENV_NAME']}")
